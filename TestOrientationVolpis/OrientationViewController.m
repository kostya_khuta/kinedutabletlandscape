//
//  OrientationViewController.m
//  TestOrientationVolpis
//
//  Created by Yatseyko Yura on 08.01.16.
//  Copyright © 2016 Yatseyko Yura. All rights reserved.
//

#import "OrientationViewController.h"

#import "LandScapeViewController.h"
#import "PortraitViewController.h"

@interface OrientationViewController ()


@property (nonatomic, strong) LandScapeViewController * landScapeView;
@property (nonatomic, strong) PortraitViewController * portraitView;

@end

@implementation OrientationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [UIApplication sharedApplication].statusBarHidden = YES;
    
    if ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeLeft || [[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeRight ) {
        self.landScapeView = [[LandScapeViewController alloc] init];
        self.landScapeView.view.autoresizingMask = UIViewAutoresizingNone;
        UIView * view = [[self landScapeView] view];
        CGRect frame = view.frame;
        frame.origin.y = 0;
        frame.size.width = CGRectGetWidth([[UIScreen mainScreen] bounds]);
        frame.size.height = self.view.frame.size.height;
        view.frame = frame;
        [self.view addSubview:view];
    } else {
        self.portraitView = [[PortraitViewController alloc] init];
        self.portraitView.view.autoresizingMask = UIViewAutoresizingNone;
        UIView * view = [[self portraitView] view];
        CGRect frame = view.frame;
        frame.origin.y = 0;
        frame.size.width = CGRectGetWidth([[UIScreen mainScreen] bounds]);
        frame.size.height = self.view.frame.size.height;
        view.frame = frame;
        [self.view addSubview:view];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration NS_DEPRECATED_IOS(2_0,8_0, "Implement viewWillTransitionToSize:withTransitionCoordinator: instead") {
    
    
    [[self.view subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        self.landScapeView = [[LandScapeViewController alloc] init];
        self.landScapeView.view.autoresizingMask = UIViewAutoresizingNone;
        UIView * view = [[self landScapeView] view];
        CGRect frame = view.frame;
        frame.origin.y = 0;
        frame.size.width = CGRectGetHeight([[UIScreen mainScreen] bounds]);
        frame.size.height = self.view.frame.size.width;
        view.frame = frame;
        [self.view addSubview:view];
    } else {
        self.portraitView = [[PortraitViewController alloc] init];
        self.portraitView.view.autoresizingMask = UIViewAutoresizingNone;
        UIView * view = [[self portraitView] view];
        CGRect frame = view.frame;
        frame.origin.y = 0;
        frame.size.width = CGRectGetHeight([[UIScreen mainScreen] bounds]);
        frame.size.height = self.view.frame.size.width;
        view.frame = frame;
        [self.view addSubview:view];
    }
}
 

@end
