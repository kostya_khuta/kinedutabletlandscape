//
//  PortraitViewController.m
//  TestOrientationVolpis
//
//  Created by Yatseyko Yura on 08.01.16.
//  Copyright © 2016 Yatseyko Yura. All rights reserved.
//

#import "PortraitViewController.h"

@interface PortraitViewController ()

@end

@implementation PortraitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fullDescriptionButton.layer.cornerRadius = 15.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
