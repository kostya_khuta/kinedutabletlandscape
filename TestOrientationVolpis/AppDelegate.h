//
//  AppDelegate.h
//  TestOrientationVolpis
//
//  Created by Yatseyko Yura on 08.01.16.
//  Copyright © 2016 Yatseyko Yura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

