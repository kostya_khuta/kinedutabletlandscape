//
//  PortraitViewController.h
//  TestOrientationVolpis
//
//  Created by Yatseyko Yura on 08.01.16.
//  Copyright © 2016 Yatseyko Yura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortraitViewController : UIViewController

#pragma mark - Properties

@property (strong, nonatomic) IBOutlet UIButton *fullDescriptionButton;

@end
