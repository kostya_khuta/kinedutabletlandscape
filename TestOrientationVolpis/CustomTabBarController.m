//
//  CustomTabBarController.m
//  TestOrientationVolpis
//
//  Created by Yatseyko Yura on 08.01.16.
//  Copyright © 2016 Yatseyko Yura. All rights reserved.
//

#import "CustomTabBarController.h"

#import "OrientationViewController.h"

@interface CustomTabBarController () {
    UIButton *button;
    UIImage *buttonImage;
}

@end

@implementation CustomTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    

    
    NSMutableArray *tabViewControllers = [[NSMutableArray alloc] init];
    [tabViewControllers addObject:[[OrientationViewController alloc] init]];
    [tabViewControllers addObject:[[OrientationViewController alloc] init]];
    [tabViewControllers addObject:[[OrientationViewController alloc] init]];
    [tabViewControllers addObject:[[OrientationViewController alloc] init]];
    [tabViewControllers addObject:[[OrientationViewController alloc] init]];
    
    [self setViewControllers:tabViewControllers];
    
    UITabBar *tabBar = self.tabBar;
    tabBar.translucent = NO;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
    
    UIImage *tabBackgroundImage = [UIImage imageNamed:@"nav_bg.png"];
    [tabBar setBackgroundImage:tabBackgroundImage];
    [tabBar setClipsToBounds:YES];

    [tabBarItem1 setTitle:@"Home"];
    [tabBarItem1 setImage:[[self image:[UIImage imageNamed:@"icon1.png"]scaledToSize:CGSizeMake(28.0f, 28.0f)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];;
    [tabBarItem1 setSelectedImage:[[self image:[UIImage imageNamed:@"icon1.png"]scaledToSize:CGSizeMake(28.0f, 28.0f)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];;

    [tabBarItem2 setTitle:@"Progress"];
    [tabBarItem2 setImage:[[self image:[UIImage imageNamed:@"icon2.png"]scaledToSize:CGSizeMake(28.0f, 28.0f)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];;
    [tabBarItem2 setSelectedImage:[[self image:[UIImage imageNamed:@"icon2.png"]scaledToSize:CGSizeMake(28.0f, 28.0f)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem3 setImage:[[self image:[UIImage imageNamed:@"baby.png"]scaledToSize:CGSizeMake(45.0f, 45.0f)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem3 setSelectedImage:[[self image:[UIImage imageNamed:@"baby.png"]scaledToSize:CGSizeMake(45.0f, 45.0f)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem4 setTitle:@"Catalogue"];
    [tabBarItem4 setImage:[[self image:[UIImage imageNamed:@"icon3.png"]scaledToSize:CGSizeMake(28.0f, 28.0f)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];;
    [tabBarItem4 setSelectedImage:[[self image:[UIImage imageNamed:@"icon3.png"]scaledToSize:CGSizeMake(28.0f, 28.0f)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem5 setTitle:@"Milestones"];
    [tabBarItem5 setImage:[[self image:[UIImage imageNamed:@"icon4.png"]scaledToSize:CGSizeMake(20.0f, 32.0f)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];;
    [tabBarItem5 setSelectedImage:[[self image:[UIImage imageNamed:@"icon4.png"]scaledToSize:CGSizeMake(20.0f, 32.0f)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor whiteColor], NSForegroundColorAttributeName ,  nil] forState:UIControlStateSelected];
    
    [tabBar setItemWidth:CGRectGetWidth([[UIScreen mainScreen] bounds])/8];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//Resize image
- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size{
    
    if (CGSizeEqualToSize(originalImage.size, size)){
        return originalImage;
    }
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}



@end
