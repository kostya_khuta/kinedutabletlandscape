//
//  main.m
//  TestOrientationVolpis
//
//  Created by Yatseyko Yura on 08.01.16.
//  Copyright © 2016 Yatseyko Yura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
